@REM ******************************************************************************
@REM * FILE PURPOSE: SA LLD Builder
@REM ******************************************************************************
@REM * FILE NAME: sabuild.bat
@REM *
@REM * DESCRIPTION: 
@REM *  The batch file cleans and builds the installed SA LLD. 
@REM *  Please make sure that the SA LLD Build Environment is setup correctly before 
@REM *  calling this file. 
@REM *
@REM *****************************************************************************

REM *****************************************************************************
REM Procedure to build using XDC. Obsolete
REM *****************************************************************************
@echo OFF

REM *****************************************************************************
REM Clean all SA LLD libraries
REM *****************************************************************************
gmake -C ti/drv/sa    clean

REM *****************************************************************************
REM Build both BE and LE libraries
REM *****************************************************************************
gmake -C ti/drv/sa    all

REM *****************************************************************************
REM  DONE
REM *****************************************************************************
