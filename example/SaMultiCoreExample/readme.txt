 ******************************************************************************
 * FILE PURPOSE: Readme File for the SA Multi process Example Project
 ******************************************************************************
 * FILE NAME: Readme.txt
 *****************************************************************************
The example demonstrates the use of SA LLD APIs to initialize the SA sub-system
on Keystone device and downlaod the PDSP images. It also demonstrates the 
use of SA LLD channel APIs to setup the SA LLD channels and send/receive packets 
in IPSec Tunnel mode. The IPSec Tunnel (Connection 0) is created on Master Core and
all other cores share the IPSec Tunnel created by master core for the communication.

Check the user guide for prerequisites, version information and steps on how to 
run the examples

