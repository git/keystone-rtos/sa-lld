/**
 *   @file  pkt_test4_enc.c
 *
 *   @brief
 *      This is the SA example test vector input file and is used by the the SA LLD examples.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2018, Texas Instruments, Inc.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdint.h>

const uint8_t pkt_test4_enc[] = { 
 
 0x00, 0xbe, 0x00, 0x22, 0x00, 0x9c, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0x00, 0xe0, 0xa6, 0x66,
 0x57, 0x04, 0x08, 0x00, 0x45, 0x00, 0x00, 0xb0, 0x00, 0x00, 0x00, 0x00, 0x05, 0x32, 0x7f, 0x02,
 0x9e, 0xda, 0x6d, 0x14, 0x14, 0x15, 0x16, 0x17, 0x44, 0x44, 0x44, 0x44, 0x12, 0x12, 0x12, 0x12,
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xaf, 0x3e, 0x0b, 0x5d, 0x69, 0xc8, 0xc3, 0x71,
 0x36, 0x01, 0x76, 0x40, 0xc3, 0x62, 0x5d, 0x8c, 0x2e, 0x89, 0xc8, 0x27, 0xe8, 0x15, 0x45, 0x63,
 0xc4, 0xc6, 0x2b, 0xc4, 0x83, 0x08, 0xec, 0x33, 0x71, 0xb7, 0xa9, 0x4c, 0xe6, 0xc0, 0xc8, 0xe9,
 0xc5, 0xfe, 0x75, 0x66, 0x8a, 0xb2, 0xfc, 0x2c, 0x5e, 0x79, 0xc2, 0x1e, 0x72, 0x34, 0x58, 0x98,
 0x5b, 0x9e, 0xd2, 0x8f, 0xe4, 0xe3, 0x7a, 0xc1, 0x9b, 0xdf, 0xec, 0x00, 0x2a, 0x77, 0x1a, 0x8f,
 0x8e, 0xc8, 0x98, 0xba, 0x53, 0xb0, 0xeb, 0x25, 0x86, 0xa5, 0x80, 0x8c, 0xbc, 0x2f, 0x94, 0x1e,
 0xf4, 0xfa, 0x9e, 0xff, 0x1c, 0xee, 0x54, 0x2b, 0x81, 0x6d, 0xf9, 0x77, 0x8b, 0x8d, 0x0b, 0x59,
 0x14, 0x3d, 0x70, 0xe5, 0x07, 0x4c, 0xa7, 0x33, 0xe0, 0xef, 0x0a, 0x64, 0xe2, 0xb6, 0x63, 0xb8,
 0x5e, 0x21, 0x35, 0x5e, 0xbb, 0x9e, 0x41, 0x7d, 0x87, 0x4c, 0xcf, 0xe0, 0x0c, 0xe8, 0xa0, 0x4a,
 0x98, 0x74, 0x30, 0x14, 0x00, 0xbe, 0x00, 0x22, 0x00, 0x9c, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc,
 0x00, 0xe0, 0xa6, 0x66, 0x57, 0x04, 0x08, 0x00, 0x45, 0x00, 0x00, 0xb0, 0x00, 0x00, 0x00, 0x00,
 0x05, 0x32, 0x7f, 0x02, 0x9e, 0xda, 0x6d, 0x14, 0x14, 0x15, 0x16, 0x17, 0x44, 0x44, 0x44, 0x44,
 0x12, 0x12, 0x12, 0x13, 0xaf, 0x3e, 0x0b, 0x5d, 0x69, 0xc8, 0xc3, 0x71, 0x9b, 0x28, 0xe7, 0xa5,
 0x9d, 0xde, 0xf1, 0xb4, 0x2b, 0xa5, 0x75, 0xf7, 0x05, 0x1b, 0x66, 0xb0, 0xb7, 0xd5, 0x79, 0xf4,
 0x00, 0xa1, 0x17, 0xf9, 0x05, 0x98, 0x65, 0xaa, 0x4e, 0x5d, 0x1a, 0xa3, 0x89, 0xf0, 0x09, 0x7d,
 0x5f, 0x5d, 0x78, 0xc2, 0x0f, 0xff, 0xc3, 0x4f, 0xab, 0x56, 0x45, 0x89, 0x2a, 0x34, 0x38, 0x15,
 0x28, 0x1b, 0x41, 0xb9, 0xe0, 0xd0, 0x55, 0x15, 0xbe, 0xca, 0x9c, 0x0c, 0xc6, 0xb7, 0xca, 0x29,
 0xb7, 0x62, 0xe1, 0xd4, 0x52, 0xbb, 0xbc, 0x3c, 0x96, 0xe4, 0x3f, 0xba, 0x48, 0x62, 0x6b, 0x71,
 0xac, 0x5e, 0x67, 0x84, 0x6b, 0x2c, 0xd2, 0xef, 0x4d, 0xae, 0x34, 0xd1, 0x08, 0x2b, 0xfd, 0x2b,
 0x2d, 0x1d, 0x06, 0x83, 0x49, 0x4a, 0x12, 0x0d, 0x93, 0xe5, 0xc5, 0xd2, 0xfd, 0x19, 0xf9, 0xfb,
 0xc8, 0x3f, 0x13, 0xf5, 0x59, 0x14, 0xac, 0x59, 0x2d, 0x2c, 0xe0, 0x5e, 0xfc, 0xf7, 0xcf, 0x73,
 0x15, 0x97, 0x30, 0xa7, 0x83, 0x2d, 0x3b, 0xef, 0x00, 0xbe, 0x00, 0x22, 0x00, 0x9c, 0x12, 0x34,
 0x56, 0x78, 0x9a, 0xbc, 0x00, 0xe0, 0xa6, 0x66, 0x57, 0x04, 0x08, 0x00, 0x45, 0x00, 0x00, 0xb0,
 0x00, 0x00, 0x00, 0x00, 0x05, 0x32, 0x7f, 0x02, 0x9e, 0xda, 0x6d, 0x14, 0x14, 0x15, 0x16, 0x17,
 0x44, 0x44, 0x44, 0x44, 0x12, 0x12, 0x12, 0x14, 0x9b, 0x28, 0xe7, 0xa5, 0x9d, 0xde, 0xf1, 0xb4,
 0x0a, 0x0e, 0xfe, 0x4b, 0x42, 0x96, 0x6b, 0x25, 0x81, 0xc6, 0xbe, 0x42, 0xcd, 0x79, 0x6b, 0x1d,
 0x86, 0x27, 0xd2, 0x1d, 0x93, 0xc0, 0x76, 0xd2, 0x00, 0xaa, 0xc6, 0x9f, 0xd9, 0xb1, 0x30, 0xae,
 0xfe, 0x38, 0xc9, 0x6e, 0x79, 0x53, 0x7b, 0xa6, 0xca, 0xab, 0x16, 0x57, 0xcf, 0x99, 0x60, 0x8e,
 0x8e, 0xa9, 0xca, 0xb2, 0xfe, 0x9e, 0xa3, 0x45, 0x1a, 0x49, 0x45, 0xbd, 0xf6, 0xbe, 0xfb, 0x5a,
 0x2e, 0x20, 0xc8, 0x15, 0x7d, 0x67, 0x07, 0x80, 0xc2, 0x39, 0x99, 0xac, 0x26, 0x55, 0x62, 0x14,
 0xcc, 0x4c, 0x5e, 0x85, 0x65, 0xae, 0x2a, 0xc2, 0x55, 0xed, 0x2c, 0x87, 0xb8, 0x8d, 0x36, 0xc9,
 0xf6, 0xf1, 0xf9, 0xf5, 0x79, 0xc8, 0x18, 0x53, 0x9f, 0x97, 0xc2, 0x16, 0x80, 0x3f, 0xeb, 0x7c,
 0xa2, 0x4a, 0xa6, 0x48, 0x85, 0x7b, 0xc3, 0xc9, 0x22, 0x8d, 0x0e, 0xd1, 0x6d, 0x2e, 0x02, 0x18,
 0x3c, 0x33, 0x18, 0xf6, 0x8d, 0xf8, 0x4a, 0x61, 0x33, 0xec, 0x63, 0x4d, 0x00, 0xbe, 0x00, 0x22,
 0x00, 0x9c, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0x00, 0xe0, 0xa6, 0x66, 0x57, 0x04, 0x08, 0x00,
 0x45, 0x00, 0x00, 0xb0, 0x00, 0x00, 0x00, 0x00, 0x05, 0x32, 0x7f, 0x02, 0x9e, 0xda, 0x6d, 0x14,
 0x14, 0x15, 0x16, 0x17, 0x44, 0x44, 0x44, 0x44, 0x12, 0x12, 0x12, 0x15, 0x0a, 0x0e, 0xfe, 0x4b,
 0x42, 0x96, 0x6b, 0x25, 0xf4, 0x57, 0x63, 0x3b, 0x95, 0xbc, 0xab, 0x17, 0x6d, 0xce, 0x42, 0x73,
 0x18, 0x62, 0x35, 0x6d, 0x5f, 0x09, 0xad, 0xb5, 0xdf, 0xeb, 0x1a, 0x8d, 0x3e, 0x5b, 0xe2, 0x89,
 0x56, 0x94, 0x3e, 0x2a, 0x78, 0x6a, 0xf7, 0xc2, 0xd9, 0xde, 0x83, 0x1f, 0x7a, 0x6e, 0x59, 0x48,
 0xf0, 0x9f, 0x90, 0xe0, 0xff, 0xa1, 0x67, 0x9d, 0xbc, 0x90, 0xf4, 0xbe, 0x06, 0x18, 0x8d, 0x2a,
 0x20, 0x3c, 0x2d, 0xe9, 0xa1, 0x12, 0x53, 0x5c, 0x13, 0x5c, 0xe3, 0x53, 0xd7, 0x06, 0xad, 0xcb,
 0x36, 0xfb, 0xdf, 0x54, 0xbf, 0x6d, 0x70, 0x7d, 0xb0, 0x70, 0xa6, 0xb5, 0xda, 0x5e, 0x4b, 0x69,
 0x1b, 0x1d, 0xf1, 0x8a, 0x56, 0xbd, 0x76, 0xe6, 0xd5, 0x16, 0xbc, 0xc7, 0xf7, 0xc0, 0xc7, 0x02,
 0xf8, 0xec, 0x1b, 0xd9, 0xd6, 0xda, 0x6d, 0x5a, 0x16, 0xd7, 0x49, 0x07, 0x70, 0xed, 0xb6, 0x25,
 0xc8, 0x86, 0x11, 0xd9, 0xb0, 0x4f, 0x98, 0xd8, 0x85, 0x32, 0x66, 0x62, 0xff, 0xba, 0xc4, 0xea,
 0x00, 0xbe, 0x00, 0x22, 0x00, 0x9c, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0x00, 0xe0, 0xa6, 0x66,
 0x57, 0x04, 0x08, 0x00, 0x45, 0x00, 0x00, 0xb0, 0x00, 0x00, 0x00, 0x00, 0x05, 0x32, 0x7f, 0x02,
 0x9e, 0xda, 0x6d, 0x14, 0x14, 0x15, 0x16, 0x17, 0x44, 0x44, 0x44, 0x44, 0x12, 0x12, 0x12, 0x16,
 0xf4, 0x57, 0x63, 0x3b, 0x95, 0xbc, 0xab, 0x17, 0x36, 0xed, 0x0f, 0x09, 0x86, 0xfd, 0x73, 0xba,
 0xd6, 0xda, 0xf1, 0x6c, 0x76, 0xcb, 0xd3, 0xb9, 0x1c, 0x15, 0x60, 0x54, 0x9f, 0x8b, 0xc5, 0xe8,
 0x0d, 0xf1, 0x0b, 0xc1, 0x04, 0x17, 0xbc, 0x9c, 0x65, 0x3e, 0xd5, 0xd7, 0x02, 0x7d, 0x92, 0x18,
 0x3c, 0x5d, 0x20, 0x4a, 0xa9, 0x55, 0xeb, 0x97, 0xbc, 0x89, 0x84, 0x50, 0x8d, 0x40, 0x13, 0x61,
 0xd1, 0xf8, 0x66, 0x74, 0xaf, 0x56, 0x68, 0xfc, 0xae, 0x35, 0x47, 0x52, 0xa8, 0xea, 0x95, 0x93,
 0x30, 0xa3, 0x05, 0x05, 0x48, 0x5b, 0x67, 0xfb, 0x94, 0x09, 0xfa, 0xeb, 0x4e, 0x22, 0x61, 0x95,
 0xd7, 0xa4, 0x1b, 0xfe, 0xf6, 0x66, 0xb0, 0x1d, 0xa1, 0xea, 0x7e, 0x56, 0x52, 0x15, 0xa0, 0x61,
 0x2f, 0x82, 0x61, 0x5f, 0x25, 0x8e, 0x52, 0xbd, 0x16, 0x09, 0x23, 0x87, 0x91, 0xc5, 0xec, 0xf0,
 0x0d, 0xef, 0xfd, 0xfe, 0x3b, 0xaf, 0x8a, 0xd2, 0xa7, 0xec, 0x02, 0xd1, 0x9e, 0xff, 0x5c, 0x55,
 0xcf, 0xf0, 0x84, 0xb4, 0x00, 0x00
};

const uint32_t pkt_test4_enc_size = sizeof (pkt_test4_enc);
