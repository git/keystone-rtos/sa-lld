 ******************************************************************************
 * FILE PURPOSE: Readme File for the SA Basic Example Project
 ******************************************************************************
 * FILE NAME: Readme.txt
 *****************************************************************************
The example demonstrates the use of SA LLD APIs to initialize the SA sub-system
on Keystone device and downlaod the PDSP images. It also demonstrates the 
use of SA LLD channel APIs to setup the SA LLD channels and send/receive packets 
in all supported security modes including IPSEC ESP/AH, SRTP and etc.

Check the user guide for prerequisites, version information and steps on how to 
run the examples

